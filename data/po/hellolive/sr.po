# 
# Translators:
# Alert Aleksandar, 2016
# Alert Aleksandar, 2016
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-12-30 06:51+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Serbian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/sr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sr\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: hellolive.html15, 29
msgid "Welcome"
msgstr "Добро дошли"

#: hellolive.html:25
msgid "Hello."
msgstr "Здраво."

#: hellolive.html:26
msgid "Thank you for downloading Ubuntu MATE."
msgstr "Хвала вам што преузимате Убунту Мејт."

#: hellolive.html:28
msgid "The"
msgstr "Програм"

#: hellolive.html:29
msgid ""
"application is your companion for getting started. Once installed, the "
"Software Boutique is available to install a selection of featured "
"applications to help you get the most out of your computing experience."
msgstr "је ваш другар приликом првих корака. Једном инсталиран, Бутик софтвера је доступан за инсталацију избора издвојених програма како би вам помогао да остварите највише искуство рачунарства."

#: hellolive.html:33
msgid "We hope you enjoy Ubuntu MATE."
msgstr "Надамо се да уживате у Убунту Мејту."

#: hellolive.html:35
msgid "Continue"
msgstr "Настави"

#: hellolive.html:28
msgid ""
"application is your companion for getting started. Once Ubuntu MATE has been"
" installed, the Software Boutique is available to install a selection of "
"featured applications to help you get the most out of your computing "
"experience."
msgstr "програм је ваш другар приликом првих корака. Једном инсталиран, Бутик софтвера је доступан за инсталацију избора издвојених програма како би вам помогао да остварите највише искуство рачунарства."

#: hellolive.html:27
msgid "This"
msgstr "Овај"

#: hellolive.html:28
msgid ""
"application is your companion for getting started with Ubuntu MATE. Once "
"Ubuntu MATE is installed, the Software Boutique is available to help you "
"install a selection of featured applications to get the most out of your "
"computing experience."
msgstr "програм је ваш другар приликом првих корака са Убунту Мејтом. Након инсталације Убунту Мејта, Бутик софтвера биће доступан као испомоћ при инсталацији избора издвојених програма како бисте остварили највише искуство у рачунарству."
